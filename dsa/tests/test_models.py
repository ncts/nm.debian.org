from django.test import TestCase
from django.core.exceptions import ValidationError
from backend.models import Person
from dsa.models import LDAPFields
import django.db


class TestLDAPFields(TestCase):
    def test_uid(self):
        person = Person.objects.create_user(
            fullname="Test", email="test@example.org", audit_skip=True
        )
        f = LDAPFields.objects.create(person=person, cn="Test", audit_skip=True)
        f.uid = "foo"
        f.full_clean()
        f.save(audit_skip=True)
        for u in (
            "foo-guest",
            "bo",
            "fooBar",
            "foo.bar",
            "foo-bar",
            "root",
        ):
            f.uid = u
            with self.assertRaises(ValidationError):
                f.full_clean()

        # Test duplicate checks on uid
        self.assertEqual(LDAPFields.objects.filter(uid="foo").count(), 1)

        person1 = Person.objects.create_user(
            fullname="Test1", email="test1@example.org", audit_skip=True
        )
        f1 = LDAPFields.objects.create(person=person1, cn="Test1", audit_skip=True)
        f1.uid = "foo"
        with self.assertRaises(ValidationError):
            f1.full_clean()
        with django.db.transaction.atomic():
            with self.assertRaises(django.db.utils.IntegrityError):
                f1.save(audit_skip=True)

        self.assertEqual(LDAPFields.objects.filter(uid="foo").count(), 1)
