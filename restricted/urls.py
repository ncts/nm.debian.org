from django.urls import path
from . import views

urlpatterns = [
    # Export database
    path('db-export/', views.DBExport.as_view(), name="restricted_db_export"),
    # Export membership information for salsa
    path('salsa-export/', views.SalsaExport.as_view(), name="export_salsa"),
]
