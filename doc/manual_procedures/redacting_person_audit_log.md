Audit log edition if needed
===========================

Following a query from a DD to remove any log on his nm account about bio
edition, I did some manual work to oblige, as there is no (yet) admin interface
for PersonAuditLog objects.

I'll summarize my manipulations here, including the obvious ones, in case they
are not so obvious for someone else.

A django shell
--------------

Here is a walkthrough of the commands I ran to get a django shell to remove what
we needed to remove:

```
ssh nono.debian.org
sudo -i -u nm
exec /bin/bash (not mandatory, but I'm not happy with dash)
cd ../nm2
./manage.py shell
```

Find your dd
------------

There are plenty ways to find the DD you're looking for. I'm quite happy using
their login, and therefore relying on the LDAP fields of a "person".

```
In [1]: from backend import models as bmodels

In [2]: my_dd = bmodels.Person.objects.get(ldap_fields__uid="dduid")

In [3]: my_dd
Out[3]: My Redacted DD <my.redacted.dd@example.org> [uid:dduid, status:dd_u]
```

Find the interesting PersonAuditLog entries
-------------------------------------------

NM defines a PersonAuditLog as an object tied to Person with a related field
name being "audit_log". Therefore finding all PersonAuditLog of a Person is
quite straightforward as soon as you have the instance of Person you need:

```
In [4]: my_dd.audit_log.all()
Out [4]: <QuerySet [REDACTED]>
```

A PersonAuditLog contains a "changes" field, which contains the litigious
elements. It's a string containing a json.

Here is how to remove the problematic changes:

```
In [5]: for entry in my_dd.audit_log.all():
   ...:     if "bio" in entry.changes:
   ...:         entry.delete()
```

This is irreversible so be sure you want to do that.

An alternative could be to edit the json to redact the unneeded entries
instead of wiping a full data. import json, json.loads(), yadda yadda.

-- 
PEB on nov the 27th 2021
