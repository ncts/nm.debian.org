from django.contrib import admin
import backend.models as bmodels


class PersonAdmin(admin.ModelAdmin):
    exclude = ("user",)
    search_fields = (
        "fullname", "email",
        *[f'ldap_fields__{x}' for x in ('cn', 'mn', 'sn', 'email', 'uid')]
    )

    def save_model(self, request, obj, form, change):
        """
        Given a model instance save it to the database.
        """
        obj.save(audit_author=request.user, audit_notes="edited from admin")


admin.site.register(bmodels.Person, PersonAdmin)


class AMAdmin(admin.ModelAdmin):
    search_fields = ("person__ldap_fields__cn", "person__ldap_fields__sn", "person__ldap_fields__email", "person__ldap_fields__uid")


admin.site.register(bmodels.AM, AMAdmin)
